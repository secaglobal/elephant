#!/bin/bash
export __DIR__=`dirname $0`
export __TEMPLATES_DIR__=${__DIR__}/../templates

if [ -z $NAMENODE_HOST ]; then
  echo "Error: NAMENODE_HOST environemnt variable is required"
  exit 1
fi

if [ !$HBASE_ROOT_DIR ] && [ $NAMENODE_HOST ]; then
  export HBASE_ROOT_DIR=hdfs://$NAMENODE_HOST:8020/hbase
fi

if [ -z $ZOOKEEPER_HOST ]; then
  echo "Error: ZOOKEEPER_HOST environemnt variable is required"
  exit 1
fi

if [ -z $HBASE_CONF_DIR ]; then
  echo "Error: HADOOP_CONF_DIR environemnt variable is required"
  exit 1
fi

sed -e "s;\${hbase.rootdir};${HBASE_ROOT_DIR};g" -e "s;\${hbase.zookeeper.quorum};${ZOOKEEPER_HOST};g" $__TEMPLATES_DIR__/hbase/hbase-site.xml |  > $HBASE_CONF_DIR/hbase-site.xml

sudo -u hdfs hdfs dfs -fs $NAMENODE_HOST:8020 -mkdir /hbase
sudo -u hdfs hdfs dfs -fs $NAMENODE_HOST:8020 -chown hbase /hbase


