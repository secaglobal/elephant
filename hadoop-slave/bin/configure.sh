#!/bin/bash
export __DIR__=`dirname $0`
export __TEMPLATES_DIR__=${__DIR__}/../templates


if [ !$HADOOP_FS_DEFAULT_FS ] && [ $NAMENODE_HOST ]; then
  export HADOOP_FS_DEFAULT_FS=hdfs://$NAMENODE_HOST
fi

if [ -z $HADOOP_FS_DEFAULT_FS ]; then
  echo "Error: HADOOP_FS_DEFAULT_FS or NAMENODE_HOST environemnt variable is required"
  exit 1
fi

if [ -z $YARN_RM_HOST ]; then
  echo "Error: YARN_RM_HOST environemnt variable is required"
  exit 1
fi

if [ -z $HADOOP_CONF_DIR ]; then
  echo "Error: HADOOP_CONF_DIR environemnt variable is required"
  exit 1
fi

sed -e "s;\${fs.defaultFS};${HADOOP_FS_DEFAULT_FS};g" $__TEMPLATES_DIR__/hadoop/core-site.xml > $HADOOP_CONF_DIR/core-site.xml
sed -e "s;\${yarn.resourcemanager.hostname};${YARN_RM_HOST};g" $__TEMPLATES_DIR__/hadoop/yarn-site.xml > $HADOOP_CONF_DIR/yarn-site.xml
