#!/bin/bash

$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_CONF_DIR/mdf.server.properties &
$KAFKA_HOME/bin/kafka-topics.sh --create --zookeeper $ZOOKEEPER_HOST --replication-factor 1 --partitions 1 --topic my-test-topic