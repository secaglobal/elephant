#!/bin/bash
export __DIR__=`dirname $0`
export __TEMPLATES_DIR__=${__DIR__}/../templates

if [ -z $ZOOKEEPER_HOST ]; then
  echo "Error: ZOOKEEPER_HOST environemnt variable is required"
  exit 1
fi

if [ -z $KAFKA_BROKER_ID ]; then
  echo "Error: KAFKA_BROKER_ID environemnt variable is required"
  exit 1
fi

if [ -z $KAFKA_HOME ]; then
  echo "Error: KAFKA_HOME environemnt variable is required"
  exit 1
fi

if [ -z $KAFKA_CONF_DIR ]; then
  echo "Error: KAFKA_CONF_DIR environemnt variable is required"
  exit 1
fi

sed -e "s;\${zookeeper.connect};${ZOOKEEPER_HOST};g" \
    -e "s;\${broker.id};${KAFKA_BROKER_ID};g" \
    $__TEMPLATES_DIR__/kafka/server.properties > $KAFKA_CONF_DIR/mdf.server.properties

