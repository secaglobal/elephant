#!/bin/bash
export __DIR__=`dirname $0`
export __TEMPLATES_DIR__=${__DIR__}/../templates

if [ -z $HADOOP_CONF_DIR ]; then
  echo "Error: HADOOP_CONF_DIR environemnt variable is required"
  exit 1
fi

cp -rf $__TEMPLATES_DIR__/hadoop/core-site.xml $HADOOP_CONF_DIR/core-site.xml
cp -rf $__TEMPLATES_DIR__/hadoop/yarn-site.xml $HADOOP_CONF_DIR/yarn-site.xml
