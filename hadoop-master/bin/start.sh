#!/bin/bash
export __DIR__=`dirname $0`
sh $__DIR__/configure.sh && \
sh $__DIR__/run.sh

sudo -u hdfs hdfs dfs -mkdir -p /var/log
sudo -u hdfs hdfs dfs -chmod -R 0777 /var

while true; do sleep 10000; done