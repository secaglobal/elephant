package com.secaglobal.elephant;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.HdfsConfiguration;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.client.api.YarnClientApplication;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.hadoop.yarn.util.Records;

import java.io.IOException;
import java.util.*;

/**
 * Starts Warehouse application
 *
 * <p>
 *     This class is entrypoint for Warehouse microservice.
 *     Warehouse is a service responsible for interaction with persistent data,
 *     located in datastores aka hdfs, mongo or mysql.
 * </p>
 *
 * TODO update desription after the goal is more clarified
 * @author levandovskiy.s@gmail.com
 * @since 28.02.2015
 */
public class Application {
    public static void main(String[] args) throws Exception {
        new Application().start();
    }

    private Configuration getYarnConfiguration() {
        YarnConfiguration conf = new YarnConfiguration();
        conf.set("yarn.resourcemanager.hostname", "192.168.59.103");

        return conf;
    }

    private Configuration getHdfsConfiguration() {
        HdfsConfiguration conf = new HdfsConfiguration();
        conf.set("fs.defaultFS", "hdfs://192.168.59.103/");

        return conf;
    }

    private void start() throws Exception {
        FileSystem fs = FileSystem.get(getHdfsConfiguration());
        Configuration yarnConf = getYarnConfiguration();
        YarnClient yarnClient = initClient(yarnConf);
        YarnClientApplication app = yarnClient.createApplication();

        Path jarLocalPath = new Path(System.getProperty("user.dir") + "/build/libs/warehouse.jar");
        String jarContainerPath = "warehouse.jar";

        ContainerLaunchContext amContext = initApplicationMasterContext(
                getAppMasterResources(fs, Collections.singletonMap(jarContainerPath, jarLocalPath)),
                getAppMasterEnvVariables(),
                getAppMasterBootCommands(yarnConf)
        );

        ApplicationSubmissionContext appContext = initApplicationContext(app, amContext);


        submitApplication(yarnClient, appContext);
    }

    private YarnClient initClient(Configuration conf) {
        YarnClient yarnClient = YarnClient.createYarnClient();
        yarnClient.init(conf);
        yarnClient.start();
        return yarnClient;
    }

    private ApplicationSubmissionContext initApplicationContext(YarnClientApplication app,
                                                                ContainerLaunchContext amContext
    ) throws IOException {

        Resource capability = Records.newRecord(Resource.class);
        capability.setMemory(2048);
        capability.setVirtualCores(1);

        ApplicationSubmissionContext appContext = app.getApplicationSubmissionContext();
        appContext.setApplicationName("simple-yarn-app");
        appContext.setAMContainerSpec(amContext);
        appContext.setResource(capability);
        appContext.setQueue("default");

        return appContext;
    }

    private ContainerLaunchContext initApplicationMasterContext(
                                        Map<String, LocalResource> resources,
                                        Map<String, String> env,
                                        List<String> commands) {

        ContainerLaunchContext amContext = Records.newRecord(ContainerLaunchContext.class);
        amContext.setLocalResources(resources);
        amContext.setEnvironment(env);
        amContext.setCommands(commands);
        amContext.setTokens(null);
        return amContext;
    }

    private Map<String, LocalResource> getAppMasterResources(
                                            FileSystem fs,
                                            Map<String, Path> filepathes) throws IOException {

        Map<String, LocalResource> map = new LinkedHashMap<>();

        for (Map.Entry<String, Path> entry: filepathes.entrySet()) {
            map.put(entry.getKey(), initLocalResource(fs, entry.getKey(), entry.getValue()));
        }

        return map;
    }

    private LocalResource initLocalResource(FileSystem fs, String containerPath, Path localPath) throws IOException {
        LocalResource resource = Records.newRecord(LocalResource.class);
        Path hdfsPath = fs.makeQualified(new Path("/jars/myapp/" + containerPath));

        fs.copyFromLocalFile(false, true, localPath, hdfsPath);

        FileStatus jarStat = fs.getFileStatus(hdfsPath);
        resource.setResource(ConverterUtils.getYarnUrlFromPath(hdfsPath));
        resource.setSize(jarStat.getLen());
        resource.setTimestamp(jarStat.getModificationTime());
        resource.setType(LocalResourceType.FILE);
        resource.setVisibility(LocalResourceVisibility.PUBLIC);

        return resource;
    }

    private Map<String, String> getAppMasterEnvVariables() {
        Map<String, String> env = new HashMap<>();
        env.put("CLASSPATH","./*:HADOOP_CONF_DIR:$HADOOP_COMMON_HOME/*:$HADOOP_COMMON_HOME/lib/*:$HADOOP_HDFS_HOME/*:$HADOOP_HDFS_HOME/lib/*:$HADOOP_MAPRED_HOME/*:$HADOOP_MAPRED_HOME/lib/*:$HADOOP_YARN_HOME/*:$HADOOP_YARN_HOME/lib/*");
        return env;
    }

    private List<String> getAppMasterBootCommands(Configuration conf) {
        String javaOptions = "-Xmx1g";
        String mainClass = "com.secaglobal.elephant.ApplicationMaster";
        String log = ApplicationConstants.LOG_DIR_EXPANSION_VAR;
        String cmdTemplate = "$JAVA_HOME/bin/java %s -Dlog4j.debug %s 1>%s/stdout 2>%s/stderr";

        return Arrays.asList(
            String.format(cmdTemplate, javaOptions, mainClass, log, log)
        );
    }

    private void submitApplication(YarnClient yarnClient,
                                   ApplicationSubmissionContext appContext
        ) throws IOException, YarnException, InterruptedException {

        ApplicationId appId = appContext.getApplicationId();
        System.out.println("Submitting application " + appId);
        yarnClient.submitApplication(appContext);

        ApplicationReport appReport = yarnClient.getApplicationReport(appId);
        YarnApplicationState appState = appReport.getYarnApplicationState();
        while (appState != YarnApplicationState.FINISHED &&
                appState != YarnApplicationState.KILLED &&
                appState != YarnApplicationState.FAILED) {

            System.out.println("Application state" + appId + " " + appState);
            Thread.sleep(100);
            appReport = yarnClient.getApplicationReport(appId);
            appState = appReport.getYarnApplicationState();
        }

        System.out.println(
                "Application " + appId + " finished with" +
                        " state " + appState +
                        " at " + appReport.getFinishTime());
    }
}
