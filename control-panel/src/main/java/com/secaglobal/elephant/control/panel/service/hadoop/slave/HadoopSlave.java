package com.secaglobal.elephant.control.panel.service.hadoop.slave;

import javax.inject.Qualifier;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Qualifier for Hadoop slave items.
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
@Target({ METHOD, CONSTRUCTOR, FIELD, TYPE })
@Retention(RUNTIME)
@Documented
@Qualifier
public @interface HadoopSlave {
}
