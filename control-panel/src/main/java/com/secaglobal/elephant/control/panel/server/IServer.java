package com.secaglobal.elephant.control.panel.server;

import com.secaglobal.elephant.control.panel.IClusterSchemaComponent;
import com.secaglobal.elephant.control.panel.service.IService;

import java.util.List;

/**
 * interface for all servers
 *
 * Every server is a placeholder for arbitrary number of {@link com.secaglobal.elephant.control.panel.service.IService}
 *
 * @see com.secaglobal.elephant.control.panel.service.IService
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface IServer extends IClusterSchemaComponent {
    /**
     * Return server connector
     * This connector can be used for getting remote access to server
     */
    public IServerConnector getConnector();

//    public Long getMemoryTotal();
//
//    public Long getMemoryMax();
//
//    public Long getMemoryUsage();

    public List<IService> getServices();
}
