package com.secaglobal.elephant.control.panel.service;

import com.secaglobal.elephant.control.panel.IClusterSchemaComponent;
import com.secaglobal.elephant.control.panel.service.hadoop.master.HadoopMasterService;
import com.secaglobal.elephant.control.panel.service.hadoop.slave.HadoopSlaveService;

import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This interface describes container abstraction.
 * Every container is a placeholder for arbitrary number of services
 * Containers responsible for operations with services, aka maintaining, updating, launching, etc.
 *
 * Container services have a declarative behavior. It means they predefined by container and not changeable,
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
@XmlSeeAlso({HadoopMasterService.class, HadoopSlaveService.class})
public interface IService extends IClusterSchemaComponent {}
