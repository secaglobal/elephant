package com.secaglobal.elephant.control.panel.server;

/**
 * Interface for all server connectors
 *
 * Connectors required for remote server access
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface ISSHConnector extends IServerConnector {
    /**
     * Ssh entry point url
     * @return entrypoint url
     */
    public String getUrl();
}
