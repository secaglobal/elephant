package com.secaglobal.elephant.control.panel.service.hadoop.master;

import com.secaglobal.elephant.control.panel.service.IService;

/**
 * Describe Hadoop Master Container interface
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface IHadoopMasterService extends IService {
}
