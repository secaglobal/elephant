package com.secaglobal.elephant.control.panel.service.hadoop.master;

import com.secaglobal.elephant.control.panel.service.AbstractService;
import com.secaglobal.elephant.control.panel.docker.IDockerable;
import com.secaglobal.elephant.control.panel.docker.IDockerImage;

import javax.inject.Inject;

/**
 * Describe Hadoop Master Container interface
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public class HadoopMasterService extends AbstractService implements IHadoopMasterService, IDockerable {
    @Inject @HadoopMaster
    private IDockerImage dockerImage;

    public IDockerImage getDockerImage() {
        return dockerImage;
    }
}
