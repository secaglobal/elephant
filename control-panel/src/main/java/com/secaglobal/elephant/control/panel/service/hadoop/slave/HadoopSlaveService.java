package com.secaglobal.elephant.control.panel.service.hadoop.slave;

import com.secaglobal.elephant.control.panel.service.AbstractService;
import com.secaglobal.elephant.control.panel.docker.IDockerable;
import com.secaglobal.elephant.control.panel.docker.IDockerImage;

import javax.inject.Inject;
import javax.xml.bind.annotation.XmlElement;

/**
 * Describe Hadoop Slave Container interface
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public class HadoopSlaveService extends AbstractService implements IHadoopSlaveService, IDockerable {
    @Inject @HadoopSlave
    private IDockerImage dockerImage;
    private String masterHost;

    @Override
    public IDockerImage getDockerImage() {
        return dockerImage;
    }

    @XmlElement(name = "master-host")
    @Override
    public String getMasterHost() {
        return masterHost;
    }

    /**
     * Sets host of master server
     * @param masterHost master server host
     */
    public void setMasterHost(String masterHost) {
        this.masterHost = masterHost;
    }
}
