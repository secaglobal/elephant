package com.secaglobal.elephant.control.panel.cluster;

import com.secaglobal.elephant.control.panel.IClusterSchemaComponent;
import com.secaglobal.elephant.control.panel.server.IServer;
import java.util.List;

/**
 * This interface describes common interface for all clusters
 *
 * Every cluster contains(describes) servers set.
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface ICluster extends IClusterSchemaComponent {

    /**
     * Cluster name
     * @return cluster name
     */
    String getName();

    /**
     * List of connected servers
     * @return servers list
     */
    List<IServer> getServers();
}
