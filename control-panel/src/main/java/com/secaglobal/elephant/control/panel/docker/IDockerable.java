package com.secaglobal.elephant.control.panel.docker;

import com.secaglobal.elephant.control.panel.docker.IDockerImage;
import com.secaglobal.elephant.control.panel.service.IService;

/**
 * This interface describes concrete Docker container abstraction.
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface IDockerable extends IService {

    /**
     * Returns docker image representation.
     * Image is a prebuilded Docker image and located in remote Docker registry or
     * local warehouse
     * @return docker image representation
     */
    public IDockerImage getDockerImage();
}
