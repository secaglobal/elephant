package com.secaglobal.elephant.control.panel.service;

import com.secaglobal.elephant.control.panel.ClusterSchemaComponentType;

/**
 * This class defines common methods for all container implementations.
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
abstract public class AbstractService implements IService {
    public final ClusterSchemaComponentType type = ClusterSchemaComponentType.Service;

    @Override
    public ClusterSchemaComponentType getType() {
        return type;
    }
}
