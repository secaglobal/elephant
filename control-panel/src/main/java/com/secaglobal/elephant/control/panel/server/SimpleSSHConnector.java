package com.secaglobal.elephant.control.panel.server;

import javax.xml.bind.annotation.XmlElement;

/**
 * Implements default ssh connector for server
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public class SimpleSSHConnector implements ISSHConnector {
    private String url;

    @XmlElement
    @Override
    public String getUrl() {
        return url;
    }

    /**
     * Sets ssh entry point url
     * @param url server ssh url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
