package com.secaglobal.elephant.control.panel.service.hadoop.slave;

import com.secaglobal.elephant.control.panel.service.IService;

/**
 * Describe Hadoop Slave Container interface
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface IHadoopSlaveService extends IService {
    public String getMasterHost();
}
