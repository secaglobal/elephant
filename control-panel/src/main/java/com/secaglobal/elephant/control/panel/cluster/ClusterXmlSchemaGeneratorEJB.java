package com.secaglobal.elephant.control.panel.cluster;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Default implementation of IClusterXmlSchemaGeneratorEJB
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
@Stateless
public class ClusterXmlSchemaGeneratorEJB implements IClusterXmlSchemaGeneratorEJB {
    //@Inject
    private ICluster type;

    @Override
    public String generateXSDSchema() {
        return "";
        //type.getClass().getCanonicalName();
    }
}
