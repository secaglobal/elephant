@XmlSchema(
    xmlns = {
        @XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance"),
        @XmlNs(prefix = "cs", namespaceURI = "http://elephant.secaglobal.com/xml/ns/cluster")
    },

    namespace = "http://elephant.secaglobal.com/xml/ns/cluster",
    location = "schemas/cluster.xsd",
    elementFormDefault = XmlNsForm.QUALIFIED
)
package com.secaglobal.elephant.control.panel.server;

import javax.xml.bind.annotation.*;