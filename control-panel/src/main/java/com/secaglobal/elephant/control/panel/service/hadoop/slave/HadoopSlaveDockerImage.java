package com.secaglobal.elephant.control.panel.service.hadoop.slave;

import com.secaglobal.elephant.control.panel.docker.IDockerImage;

/**
 * Describe Docker Image for Hadoop Slave Container
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
@HadoopSlave
public class HadoopSlaveDockerImage implements IDockerImage {
    String name = "elephant-hadoop-slave";
    String tag = "latest";

    /**
     * Unique image name
     * @return image name
     */
    public String getName() {
        return name;
    }

    /**
     * Tag name
     * @return tag name
     */
    public String getTagName() {
        return tag;
    }
}
