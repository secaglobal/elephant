package com.secaglobal.elephant.control.panel.server;

import com.secaglobal.elephant.control.panel.ClusterSchemaComponentType;

/**
 * Implements common methods for server implementations
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.29
 */
abstract public class AbstractServer implements IServer {
    private final ClusterSchemaComponentType type = ClusterSchemaComponentType.Server;

    @Override
    public ClusterSchemaComponentType getType() {
        return type;
    }
}
