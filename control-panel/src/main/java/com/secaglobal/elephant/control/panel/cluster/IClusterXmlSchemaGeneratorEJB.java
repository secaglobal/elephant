package com.secaglobal.elephant.control.panel.cluster;

/**
 * Describes interface for xml generator ejb
 * The single responsibility is generation an xml schemas for cluster representation
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface IClusterXmlSchemaGeneratorEJB {
    /**
     * Generate xsd schema
     * @return
     */
    public String generateXSDSchema();
}
