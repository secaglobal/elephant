package com.secaglobal.elephant.control.panel.server;

import com.secaglobal.elephant.control.panel.service.IService;
import com.secaglobal.elephant.control.panel.service.hadoop.master.HadoopMasterService;
import com.secaglobal.elephant.control.panel.service.hadoop.slave.HadoopSlaveService;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Implements simple bare metal server
 * Assumes that this type of server cover the most cases to interect with remote server
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.29
 */
public class SimpleBareMetalServer extends AbstractServer {
    private IServerConnector connector;
    private List<IService> services;

    @XmlElements({
        @XmlElement(type = SimpleSSHConnector.class, name = "ssh-connector")
    })
    @Override
    public IServerConnector getConnector() {
        return connector;
    }

    /**
     * Sets server connector
     * This connector will be used for getting remote access to server
     * @param connector server connector
     */
    public void setConnector(IServerConnector connector) {
        this.connector = connector;
    }

    @XmlElementWrapper
    @XmlElements({
        @XmlElement(type = HadoopMasterService.class, name = "hadoop-master"),
        @XmlElement(type = HadoopSlaveService.class, name = "hadoop-slave")
    })
    @Override
    public List<IService> getServices() {
        return services;
    }

    /**
     * Sets server servies list
     * @param services server services
     */
    public void setServices(List<IService> services) {
        this.services = services;
    }
}
