package com.secaglobal.elephant.control.panel.cluster;

import com.secaglobal.elephant.control.panel.ClusterSchemaComponentType;

/**
 * Abstract class for implementation of ICluster interface
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
abstract public class AbstractCluster implements ICluster {
    final private  ClusterSchemaComponentType type = ClusterSchemaComponentType.Cluster;

    @Override
    public ClusterSchemaComponentType getType() {
        return type;
    }
}
