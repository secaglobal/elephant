package com.secaglobal.elephant.control.panel.docker;

/**
 * This interface describes common interface for docker images
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface IDockerImage {

    /**
     * Unique image name
     * @return image name
     */
    public String getName();

    /**
     * Tag name
     * @return tag name
     */
    public String getTagName();
}
