package com.secaglobal.elephant.control.panel.service.hadoop.master;

import com.secaglobal.elephant.control.panel.docker.IDockerImage;

/**
 * Describe Docker Image for Hadoop Master Container
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
@HadoopMaster
public class HadoopMasterDockerImage implements IDockerImage {
    String name = "elephant-hadoop-master";
    String tag = "latest";

    /**
     * Unique image name
     * @return image name
     */
    public String getName() {
        return name;
    }

    /**
     * Tag name
     * @return tag name
     */
    public String getTagName() {
        return tag;
    }
}
