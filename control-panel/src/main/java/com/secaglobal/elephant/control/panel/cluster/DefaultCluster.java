package com.secaglobal.elephant.control.panel.cluster;

import com.secaglobal.elephant.control.panel.server.IServer;
import com.secaglobal.elephant.control.panel.server.SimpleBareMetalServer;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * The default implementation of ICluster interface
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
@XmlRootElement(name = "cluster")
public class DefaultCluster extends AbstractCluster {
    private String name;
    private List<IServer> servers;

    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets name / title of cluster
     * @param name Name of cluster
     */
    public void setName(String name) {
        this.name = name;
    }

    @XmlElementWrapper(name = "servers")
    @XmlElements({
        @XmlElement(type = SimpleBareMetalServer.class, name = "bare-metal")
    })
    @Override
    public List<IServer> getServers() {
        return servers;
    }

    /**
     * Assign servers to cluster
     * @param servers List of servers
     */
    public void setServers(List<IServer> servers) {
        this.servers = servers;
    }
}
