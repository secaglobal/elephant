package com.secaglobal.elephant.control.panel.cluster;

import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;

/**
 * Implements default / simple cluster unmarshaller interface
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.04.05
 */
public class DefaultClusterUnmarshaller implements IClusterUnmarshaller {
    private Schema schema;

    public DefaultClusterUnmarshaller() throws URISyntaxException, SAXException, JAXBException {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        schema = sf.newSchema(new File(ClassLoader.getSystemResource("schemas/cluster.xsd").toURI()));
    }

    @Override
    public ICluster unmarshal(InputStream stream, Class<? extends ICluster> clusterClass) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(clusterClass);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setSchema(schema);
        unmarshaller.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
        return  clusterClass.cast(unmarshaller.unmarshal(stream));
    }
}
