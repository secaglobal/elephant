package com.secaglobal.elephant.control.panel.cluster;

import java.io.InputStream;

/**
 * Describes cluster unmarshaller interface
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.04.05
 */
public interface IClusterUnmarshaller {
    public ICluster unmarshal(InputStream stream, Class<? extends ICluster> clusterClass) throws Exception;
}
