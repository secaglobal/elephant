package com.secaglobal.elephant.control.panel;

/**
 * This interface describes common interface for all clusters schema elements. Aka cluster, server, etc
 *
 * Every cluster contains(describes) servers set.
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public interface IClusterSchemaComponent {
    /**
     * Type of element
     * @return
     */
    public ClusterSchemaComponentType getType();
}
