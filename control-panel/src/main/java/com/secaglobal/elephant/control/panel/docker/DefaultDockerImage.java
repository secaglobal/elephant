package com.secaglobal.elephant.control.panel.docker;

/**
 * This interface describes common interface for docker images
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public class DefaultDockerImage implements IDockerImage {
    private String name;
    private String tag = "latest";

    /**
     * Unique image name
     * @return image name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Tag name
     * @return tag name
     */
    @Override
    public String getTagName() {
        return tag;
    }
}
