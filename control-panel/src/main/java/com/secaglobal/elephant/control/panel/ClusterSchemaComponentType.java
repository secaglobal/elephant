package com.secaglobal.elephant.control.panel;

/**
 * This is types of all available Cluster Schema Elements
 *
 * Every cluster contains(describes) servers set.
 *
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public enum ClusterSchemaComponentType {
    Cluster, Server, Service, Link
}