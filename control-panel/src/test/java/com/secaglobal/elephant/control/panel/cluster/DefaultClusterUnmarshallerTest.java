package com.secaglobal.elephant.control.panel.cluster;

import com.secaglobal.elephant.control.panel.server.ISSHConnector;
import com.secaglobal.elephant.control.panel.server.SimpleBareMetalServer;
import com.secaglobal.elephant.control.panel.service.IService;
import com.secaglobal.elephant.control.panel.service.hadoop.master.IHadoopMasterService;

import com.secaglobal.elephant.control.panel.service.hadoop.slave.IHadoopSlaveService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.hamcrest.CoreMatchers.instanceOf;

import java.util.List;

import static com.secaglobal.elephant.Context.cdiContext;

/**
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public class DefaultClusterUnmarshallerTest {
    ICluster cluster;
    String clusterConfigPath = "com/secaglobal/elephant/control/panel/cluster/DefaultClusterUnmarshallerTest/cluster.xml";

    @BeforeClass
    public void beforeClass() throws Exception {
        IClusterUnmarshaller unmarshaller = cdiContext.instance().select(DefaultClusterUnmarshaller.class).get();
        cluster = unmarshaller.unmarshal(ClassLoader.getSystemResourceAsStream(clusterConfigPath), DefaultCluster.class);
    }

    @Test(description = "#unmarshal should parse cluster metadata")
    public void unmarshalShouldParseClusterMetadata() throws Exception {
        Assert.assertEquals(cluster.getName(), "Test cluster");
    }

    @Test(description = "#unmarshal should parse servers configuration")
    public void unmarshalShouldParseServersConfiguration() throws Exception {
        Assert.assertEquals(cluster.getServers().size(), 2);
    }

    @Test(description = "#unmarshal should parse simple bare metal servers configuration")
    public void unmarshalShouldParseSimpleBareMetalServersConfiguration() throws Exception {
        Assert.assertEquals(cluster.getServers().get(0).getClass(), SimpleBareMetalServer.class);
        Assert.assertEquals(cluster.getServers().get(1).getClass(), SimpleBareMetalServer.class);
    }

    @Test(description = "#unmarshal should parse server connector configuration")
    public void unmarshalShouldParseServerConnectorConfiguration() throws Exception {
        ISSHConnector connector = (ISSHConnector) cluster.getServers().get(1).getConnector();
        Assert.assertEquals(connector.getUrl(), "slev@server2");
    }

    @Test(description = "#unmarshal should parse services configuration")
    public void unmarshalShouldParseServicesConfiguration() throws Exception {
        List<IService> services = cluster.getServers().get(0).getServices();
        org.hamcrest.MatcherAssert.assertThat(services.get(0), instanceOf(IHadoopMasterService.class));
        org.hamcrest.MatcherAssert.assertThat(services.get(1), instanceOf(IHadoopSlaveService.class));
        Assert.assertEquals(IHadoopSlaveService.class.cast(services.get(1)).getMasterHost(), "localhost");
    }
}
