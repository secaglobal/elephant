package com.secaglobal.elephant.control.panel.cluster;

import static  com.secaglobal.elephant.Context.*;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

/**
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public class ClusterXmlSchemaGeneratorEJBTest {
    ClusterXmlSchemaGeneratorEJB generator;

    @BeforeTest
    public void beforeTest() {
        generator = cdiContext.instance().select(ClusterXmlSchemaGeneratorEJB.class).get();
    }


    @Test(description = "#generateXSDSchema should generate XSD Schema")
    public void generateXSDSchemaShouldGenerateXSDSchema() throws Exception {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = sf.newSchema(new File(ClassLoader.getSystemResource("schemas/cluster.xsd").toURI()));

        JAXBContext context = JAXBContext.newInstance(DefaultCluster.class);
//
//        Marshaller marshaller = context.createMarshaller();
//        marshaller.setSchema(schema);
//        DefaultCluster cluster = new DefaultCluster();
//        cluster.setName("bbb");
//        marshaller.marshal(cluster, System.out);


        Unmarshaller unmarshaller = context.createUnmarshaller();

        unmarshaller.setSchema(schema);
        ICluster cluster = (ICluster) unmarshaller.unmarshal(new File(ClassLoader.getSystemResource("schemas/test.xml").toURI()));

        Assert.assertEquals(cluster, "");
        //Assert.assertEquals("file1.config.value", "file1.config.value");
    }
}
