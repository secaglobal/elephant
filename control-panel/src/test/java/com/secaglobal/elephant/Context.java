package com.secaglobal.elephant;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 * @author lev <levandovskiy.s@gmail.com>
 * @since 2015.03.28
 */
public class Context {
    public static WeldContainer cdiContext;

    static {
        Weld weld = new Weld();
        cdiContext = weld.initialize();
    }
}
