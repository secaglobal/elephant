Installation
===================

Gradle
-------------------

### Ububtu ###

    sudo add-apt-repository ppa:cwchien/gradle
    sudo apt-get update
    sudo apt-get install gradle

Ansible
-------------------
### Ububtu ###

    sudo apt-get install software-properties-common
    sudo apt-add-repository ppa:ansible/ansible
    sudo apt-get update
    sudo apt-get install ansible

### Mac OSX ###

    brew install ansible

Runing
===================

Ansible
-------------------

    ansible all -m ping -i ansible-hosts --private-key ~/keys/ubix-dev.pem -u ubuntu
    sudo ansible-galaxy install angstwad.docker_ubuntu