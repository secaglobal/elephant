package com.secaglobal.elephant.common.configuration.impl;

import com.secaglobal.elephant.common.configuration.ConfigurationService;
import com.typesafe.config.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;

/**
 * Implements {@link ConfigurationService @ConfigurationService}
 *
 * <p>
 *     Actually it is wrapper for {@link com.typesafe.config.ConfigFactory @com.typesafe.config.ConfigFactory}
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 * @see ConfigurationService
 * @see com.typesafe.config.ConfigFactory
 */
public class TypesafeConfigurationService implements ConfigurationService {
    private final Logger logger = LoggerFactory.getLogger(TypesafeConfigurationService.class);
    private Config config;

    public TypesafeConfigurationService() {
        logger.info("Configuration initialization");

        config =  ConfigFactory.load();
    }

    /**
     * Merge additional config file
     * <p>This method load specified configuration file and merge it with current configuration</p>
     * @param path Path to file. Looking at classpath
     * @see com.typesafe.config.ConfigFactory#load(com.typesafe.config.Config)
     * @see com.typesafe.config.Config#withFallback(com.typesafe.config.ConfigMergeable)
     *
     * TODO UnitTest
     */
    @Override
    public ConfigurationService mergeWith(String path) {
        logger.info("Load configuration file: {}", path);
        config = ConfigFactory.load(path).withFallback(config);
        return this;
    }

    /**
     * Merge additional config files
     * <p>This method load specified configuration files and merge it with current configuration</p>
     * @param paths Paths to files. Looking at classpath
     * @see #mergeWith
     *
     * TODO UnitTest
     */
    @Override
    public ConfigurationService mergeWith(String... paths) {
        for (String path: paths) {
            mergeWith(path);
        }

        return this;
    }

    /**
     * Convert config implementation to properties
     * <p>Convert {@link com.typesafe.config.Config @Config} implementation into {@link java.util.Properties @Properties}.
     * Useful if concrete class / service supports only Properties<p/>
     * @return Config as Properties
     *
     * TODO UnitTest
     */
    @Override
    public Properties asProperties() {
        Properties props =  new Properties();

        logger.trace("Conversion config to properties using asProperties");

        for (Map.Entry<String, ConfigValue> entry: config.entrySet()) {
            if (logger.isTraceEnabled()) {
                logger.trace("Property: {} = {}", entry.getKey(), entry.getValue().unwrapped().toString());
            }

            props.setProperty(entry.getKey(), entry.getValue().unwrapped().toString());
        }

        return props;
    }

    /**
     * Return property
     * <p>This method extract property value and returns it as required type<p/>
     * @param name key of parameter
     * @param type Type of required property
     * @param <T> Type of required property
     * @return Required property. Do not check property type and required type.
     *
     * TODO UnitTest
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T getProperty(String name, Class<T> type) {
        return (T) config.getValue(name).unwrapped();
    }

    /**
     * Add property to config
     * @param name Property name
     * @param value Value
     * @param <T> Type of required property.
     *
     * TODO UnitTest
     */
    @Override
    public <T> void setProperty(String name, final T value) {
        logger.info("Set custom config property: {} = ", name, value);
        config = config.withValue(name, ConfigValueFactory.fromAnyRef(value));
    }
}
