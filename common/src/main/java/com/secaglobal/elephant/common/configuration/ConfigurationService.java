package com.secaglobal.elephant.common.configuration;

import java.util.Properties;

/**
 * Compose all application configurations together.
 *
 * <p>
 *     This service class load all specified configuration files and compose it in one placeholder.
 * </p>
 * <p></>Can be used for Spring context properties. Example for application context configuration:
 *
 * <code>
 *     <bean id="configService" class="com.secaglobal.twitter.mesh.common.services.config.ConfigService">
 *       <property name="configLocation" value="META-INF/configuration/application-default.conf"/>
 *     </bean>
 *
 *     <bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
 *       <property name="propertiesArray">
 *         <list>
 *           <bean factory-bean="configService" factory-method="asProperties" />
 *         </list>
 *       </property>
 *     </bean>
 * </code>
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public interface ConfigurationService {

    /**
     * Merge additional config file
     * <p>This method load specified configuration file and merge it with current configuration</p>
     * @param path Path to file. Looking at classpath
     */
    public ConfigurationService mergeWith(String path);

    /**
     * Merge additional config files
     * <p>This method load specified configuration files and merge it with current configuration</p>
     * @param paths Paths to files. Looking at classpath
     */
    public ConfigurationService mergeWith(String... paths);


    /**
     * Convert config implementation to properties
     * <p>Convert ConfigService implementation into {@link java.util.Properties @Properties}.
     * Useful if concrete class / service supports only Properties<p/>
     * @return Config as Properties
     */
    public Properties asProperties();


    /**
     * Return property
     * <p>This method extract property value and returns it as required type<p/>
     * @param name key of parameter
     * @param type Type of required property
     * @param <T> Type of required property
     * @return required property
     */
    public <T> T getProperty(String name, Class<T> type);

    /**
     * Add property to config
     * @param name Property name
     * @param value Value
     * @param <T> Type of required property.
     */
    public <T> void setProperty(String name, T value);
}
