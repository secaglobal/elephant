package com.secaglobal.elephant.common.configuration.impl;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Properties;

public class TypesafeConfigurationServiceTest {
    private static String resourcesDir;
    private TypesafeConfigurationService config;

    @BeforeClass
    public static void beforeClass() {
        resourcesDir = TypesafeConfigurationServiceTest.class.getName().replace('.', File.separatorChar);
    }

    @BeforeTest
    public void beforeTest() {
        this.config = new TypesafeConfigurationService();
    }

    @Test(description = "#mergeWith should read config from file")
    public void mergeWithShouldReadConfigFromFile() {
        this.config.mergeWith(resourcesDir + "/file1.conf");
        String prop = this.config.getProperty("file1.config.name", String.class);
        Assert.assertEquals(prop, "file1.config.value");
    }

    @Test(description = "#mergeWith should merge arbitrary number of files")
    public void mergeWithShouldMergeArbitraryNumberOfFiles() {
        this.config.mergeWith(resourcesDir + "/file1.conf")
                   .mergeWith(resourcesDir + "/file2.conf");

        String prop1 = this.config.getProperty("file1.config.name", String.class);
        String prop2 = this.config.getProperty("file2.config.name", String.class);
        Assert.assertEquals(prop1, "file1.config.value");
        Assert.assertEquals(prop2, "file2.config.value");
    }

    @Test(description = "#mergeWith should override values")
    public void mergeWithShouldOverrideValues() {
        this.config.mergeWith(resourcesDir + "/file1.conf")
                   .mergeWith(resourcesDir + "/file2.conf");

        String prop = this.config.getProperty("common.config.name", String.class);
        Assert.assertEquals(prop, "common.file2.config.value");
    }

    @Test(description = "#mergeWith should support multifile input")
    public void mergeWithShouldSupportMultifileInput() {
        this.config.mergeWith(resourcesDir + "/file1.conf", resourcesDir + "/file2.conf");

        String prop1 = this.config.getProperty("file1.config.name", String.class);
        String prop2 = this.config.getProperty("file2.config.name", String.class);
        Assert.assertEquals(prop1, "file1.config.value");
        Assert.assertEquals(prop2, "file2.config.value");
    }

    @Test(description = "#setProperty should extend configuration with specified property")
    public void setPropertyShouldExtendConfigurationWithSpecifiedProperty() {
        this.config.mergeWith(resourcesDir + "/file1.conf").setProperty("nofile.config.name", "myvalue");

        String prop1 = this.config.getProperty("file1.config.name", String.class);
        String prop2 = this.config.getProperty("nofile.config.name", String.class);
        Assert.assertEquals(prop1, "file1.config.value");
        Assert.assertEquals(prop2, "myvalue");
    }

    @Test(description = "#setProperty should override value")
    public void setPropertyShouldOverrideValue() {
        this.config.mergeWith(resourcesDir + "/file1.conf").setProperty("file1.config.name", "myvalue");

        String prop1 = this.config.getProperty("file1.config.name", String.class);
        Assert.assertEquals(prop1, "myvalue");
    }

    @Test(description = "#asProperties should convert config to properties collection")
    public void asPropertiesShouldConvertConfigToPropertiesCollection() {
        Properties props =  this.config.mergeWith(resourcesDir + "/file1.conf")
                                       .mergeWith(resourcesDir + "/file2.conf")
                                       .asProperties();

        MatcherAssert.assertThat(props.size(), Matchers.greaterThan(20));
        Assert.assertEquals(props.getProperty("common.config.name"), "common.file2.config.value");
    }
}
